package com.example.balda.Utils;

import java.util.Random;

public class AI {
    private static int DEFAULT_LENGTH;

    private int curLength;

    public static int getDefaultLength() {
        Random rand = new Random();
//        DEFAULT_LENGTH = 3;
            DEFAULT_LENGTH = rand.nextInt(5)+2;
        return DEFAULT_LENGTH;


    }

    public int getCurLength() {
        return curLength;
    }

    public void incCurLength() {
        this.curLength++;
    }
    public void resetCurLength() {
        this.curLength = 0;
    }
}
