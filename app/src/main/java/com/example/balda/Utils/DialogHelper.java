package com.example.balda.Utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.balda.MainActivity;
import com.example.balda.R;

/**
 * Created by Alox1d on 16.04.2018.
 */

public class DialogHelper {
    private Context context;

    private LayoutInflater factory;
    private AlertDialog.Builder builder;
    private View view;
    private Dialog dialog;


    public DialogHelper(Context context) {
        this.context = context;
    }

    public void initializeAboutDialog() {
        builder = new AlertDialog.Builder(context);
        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_about_program, null);
        builder.setView(view);
        builder.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {

            }
        });
        builder.show();
    }

    public void initializeChoiceDialog() {
        builder = new AlertDialog.Builder(context);
        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_about_program, null);
        builder.setView(view);
        builder.setNegativeButton("В конец", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {
            }
        });
        builder.setPositiveButton("В начало", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {

            }
        });
        builder.show();
    }


    public void initializeRulesDialog() {
        builder = new AlertDialog.Builder(context);
        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_rules, null);
        builder.setView(view);
        builder.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {

            }
        });
        builder.show();
    }

    public void initializeVictoryDialog(String winText) {
        builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("Главное меню", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dlg, int sumthin) {
                Intent intent = new Intent(context, MainActivity.class);
                //intent.putStringArrayListExtra("words", words);
                context.startActivity(intent);
            }
        });
        AlertDialog alertDialog = builder.create();
        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_victory, null);
        alertDialog.setView(view);
        alertDialog.setCancelable(false);


        alertDialog.show();
        TextView tv = (TextView) alertDialog.findViewById(R.id.tv_player_win);
        tv.setText(winText);
    }
}
