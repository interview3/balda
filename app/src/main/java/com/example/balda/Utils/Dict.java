package com.example.balda.Utils;

import android.content.Context;

import com.example.balda.Models.Tree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Dict {

//    private  static Dict instance;
//    private Dict(){}
//    public static Dict getInstance(){
//        if (instance == null){
//            instance = new Dict();
//        }
//        return instance;
//    }
//     ArrayList<String> words;
//    public static Trie trie;
public static Tree tree;
public static ArrayList<String> list;
    public static ArrayList<String> getWords(Context context) {
        try {
            if (list != null) return list;
            list = new ArrayList<String>();
            tree = new Tree();
            InputStream is = context.getAssets().open("Dict_rus.txt");
            BufferedReader myDIS = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            //DataInputStream myDIS = new DataInputStream(is);
            String myLine; //declare a variable

            while ((myLine = myDIS.readLine()) != null) {
//                ArrayList<Character> chars = new ArrayList<Character>();
//                for (char c : myLine.toCharArray()) {
//                    chars.add(c);
//                }
                tree.insert(myLine.toCharArray());
                list.add(myLine);
            }


//            int size = is.available();
//            // Read the entire asset into a local byte buffer.
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            // Convert the buffer into a string.
//            String text = new String(buffer);
//            boolean nashel = tree.findPref("ЫЫ");
//            boolean nashel2 = tree.findPref("РЫ");
//            boolean nashel4 = tree.findPref("РЫБА");
//            boolean nashel5 = tree.findPref("РЫБАК");
//            boolean nashel6 = tree.findPref("РЫБАКИ");
//            boolean nashel11 = tree.find("ЫЫ");
//            boolean nashel21 = tree.find("РЫ");
//            boolean nashel41 = tree.find("РЫБА");
//            boolean nashel51 = tree.find("РЫБАК");
//            boolean nashel61 = tree.find("РЫБАКИ");
            is.close();
            return list;

        } catch (IOException e) {

            throw new RuntimeException(e);
        }
    }
}
