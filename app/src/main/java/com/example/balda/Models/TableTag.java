package com.example.balda.Models;

public class TableTag {
    int i;
    int j;

    public TableTag(int i, int j) {
        this.i = i;
        this.j = j;

    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }
}
