package com.example.balda.Models;

import java.util.ArrayList;

public class Tree {
    Node root;


    public boolean findPref(String pref){
        char[] chars = pref.toCharArray();
        Node current = root;
        boolean isFound = false;
        int iCurFound = 0;
        for (int i = 0; i<chars.length; i++){
            for (int j = 0; j<current.data.length; j++){
                if (current.data[j] == chars[i]){
                    iCurFound = j;
                    isFound = true;
                }
            }
            if (isFound){
                current = current.childs[iCurFound];
                isFound = false;
            } else {
                return false;
            }
//            if (!current.data.contains(chars[i])) return false;
//            current = current.childs;
        }
        return true;
    }
//    public boolean find(String str){
//        char[] chars = str.toCharArray();
//        Node current = root;
//        boolean isFound = false;
//        int iCurFound = 0;
//        for (int i = 0; i<chars.length; i++){
//            for (int j = 0; j<current.data.length; j++){
//                if (current.data[j] == chars[i]){
//                    iCurFound = j;
//                    isFound = true;
//                }
//            }
//            if (isFound){
//                current = current.childs[iCurFound];
//                isFound = false;
//            } else {
//                return false;
//            }
//        }
//        if (current.data[0] == '\u0000') return true;
//        return false;
//    }


    public void insert(char[] data){
        Node current = root;
        Node prev = null;
        for (int i = 0; i<data.length; i++){
            Node node = new Node();
//            node.key = key;
            node.data = new char[33];
            node.childs = new Node[33];
//            node.childs.data =  new ArrayList<>();
//            node.childs = new ArrayList<>();
            if(root==null){
                root = node;
                root.data[i] = data[i];
                root.childs[i] = new Node();
                root.childs[i].childs = new Node[33];
                root.childs[i].data = new char[33];
                current = root.childs[i];
//                int curInd = root.data.indexOf(data.get(i));

//                current = root.childs;
//                if (!(root.data.indexOf(data.get(i))>=0)){
//
//                }
            }else{
                boolean isAlreadyAdded = false;
                int iCur = 0;
                for (int j = 0; j<current.data.length; j++){
                    if (current.data[j] == '\u0000'){
                        iCur = j;
                        break;
                    }
                    if(current.data[j] == data[i]){
                        iCur = j;
                        isAlreadyAdded = true;
                        break;
                    }


                }
                if (!isAlreadyAdded){
                    current.data[iCur] = data[i];
                }
                if (current.childs[iCur] == null) current.childs[iCur] = node;
                current = current.childs[iCur];
            }
        }


    }


//    public void print(Node startNode){
//        if(startNode != null){//условие сработает, когда мы достигним конца дерева и потомков не останется
//            print(startNode.leftChild);//рекурсивно вызываем левых потомков
//            startNode.printNode();//вызов метода принт
//            print(startNode.rightChild);//вызов правых
//        }
//    }
}
