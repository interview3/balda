package com.example.balda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balda.Models.TableTag;
import com.example.balda.Utils.DialogHelper;
import com.example.balda.Utils.Dict;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;
import java.util.Random;

public class TwoPlayersActivity extends AppCompatActivity {
    String input_string = "";
    ArrayList<Button> pressedBtns = new ArrayList<>();
    ArrayList<Character> inputChars = new ArrayList<>();
    boolean playerOneTurn;
    ArrayAdapter<String> adapterP1;
    ArrayAdapter<String> adapterP2;
    ArrayList<String> listItemsP1 = new ArrayList<String>();
    ArrayList<String> listItemsP2 = new ArrayList<String>();
    int scoreP1 = 0;
    int scoreP2 = 0;
    Button selectedBtn;
    String originalWord;
    TextView scoreP1TV;
    TextView scoreP2TV;
    TableTag previousTag;
    boolean isAddBlocked;
    TextView tvPlayerMove;
    boolean isLetterAdded;
    int countOfBusyCells;
    ArrayList<ArrayList<Button>> btns = new ArrayList<>();
    int sizeOriginWord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        initializeUI();

        countOfBusyCells = sizeOriginWord;
//        try {
//            InputStream is = getAssets().open("Dict_rus.txt");
//            BufferedReader myDIS = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//            //DataInputStream myDIS = new DataInputStream(is);
//            ArrayList<String> list = new ArrayList<String>();
//            String myLine; //declare a variable
//
//            while((myLine=myDIS.readLine())!=null) list.add(myLine);
//
//
//
////            int size = is.available();
////            // Read the entire asset into a local byte buffer.
////            byte[] buffer = new byte[size];
////            is.read(buffer);
////            // Convert the buffer into a string.
////            String text = new String(buffer);
//            is.close();
//
//
//        } catch (IOException e) {
//            // Should never happen!
//            throw new RuntimeException(e);
//        }

    }

    private void initializeUI() {
        adapterP1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItemsP1);
        adapterP2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItemsP2);
        ListView listView = findViewById(R.id.used_words_p1);
        listView.setAdapter(adapterP1);
        listView = findViewById(R.id.used_words_computer);
        listView.setAdapter(adapterP2);

        scoreP1TV = findViewById(R.id.player_1_score);
        scoreP2TV = findViewById(R.id.computer_score);

        Random rand = new Random();
        playerOneTurn = rand.nextBoolean();
        tvPlayerMove = findViewById(R.id.tv_player_move);
        if (playerOneTurn) {
            tvPlayerMove.setText("Ход игрока 1");
            Toast.makeText(this, "Ход игрока 1", Toast.LENGTH_LONG).show();
        } else {
            tvPlayerMove.setText("Ход игрока 2");
            Toast.makeText(this, "Ход игрока 2", Toast.LENGTH_LONG).show();

        }
        Intent intent = getIntent();
        originalWord = intent.getStringExtra("word");
        sizeOriginWord = originalWord.length();
        char[] word_array = originalWord.toCharArray();


        FlexboxLayout parentGameFB = findViewById(R.id.parent_game_fb);
        FlexboxLayout flexboxRow = new FlexboxLayout(this);
        FlexboxLayout.LayoutParams params = new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT);
        flexboxRow.setLayoutParams(params);
        flexboxRow.setFlexWrap(FlexWrap.NOWRAP);
        int buttonStyle = R.style.Widget_AppCompat_Button_Colored;

        for (int i = 0; i < sizeOriginWord; i++) {
            ArrayList<Button> btnRow = new ArrayList<>();
            for (int j = 0; j < sizeOriginWord; j++) {
                btnRow.add(createCell(sizeOriginWord, word_array, flexboxRow, buttonStyle, i, j));
            }
            parentGameFB.addView(flexboxRow);
            flexboxRow = new FlexboxLayout(this);
            flexboxRow.setLayoutParams(params);
            flexboxRow.setFlexWrap(FlexWrap.NOWRAP);

            btns.add(btnRow);

        }
        Button OK = findViewById(R.id.btn_OK);
        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(countOfBusyCells == sizeOriginWord * sizeOriginWord)) {

                    if (isLetterAdded) {
                        if(!addWord()) clearSelectedBtn();
                        cancelSelect();
                        nextTurn();
                        nextPlayer();
                    } else {
                        Toast.makeText(v.getContext(), "Вы не добавили букву!", Toast.LENGTH_LONG).show();
                    }

                } else

                {
                    displayVictory(v.getContext());
                }
            }

        });
        Button reset = findViewById(R.id.btn_reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelectedBtn();
                cancelSelect();
            }
        });
        Button sur = findViewById(R.id.btn_sur);
        sur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper dialogHelper = new DialogHelper(v.getContext());
                displayVictoryByMember();


            }
        });
    }

    private Button createCell(int size, char[] word_array, FlexboxLayout flexboxRow, int buttonStyle, int i, int j) {
        final Button btn = new Button(new ContextThemeWrapper(this, buttonStyle), null, buttonStyle);
        FlexboxLayout.LayoutParams btnParams = new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT);
        btn.setLayoutParams(btnParams);
        btn.setGravity(Gravity.CENTER);
        btn.setTag(new TableTag(i, j));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button) v;
                TableTag tag = (TableTag) btn.getTag();
                int currentStr = tag.getI();
                int currentCol = tag.getJ();
                                if (isAnyNearEmptyCell(currentStr, currentCol))
                if (btn.getText().toString().isEmpty() && !isAddBlocked) {
//                            if ((Math.abs(tag.getI() - previousTag.getI()) == 1 || Math.abs(tag.getJ() - previousTag.getJ()) == 1)
//                                    && (!(Math.abs(tag.getI() - previousTag.getI()) == 1) || !(Math.abs(tag.getJ() - previousTag.getJ()) == 1))) { // XOR
                    cancelSelect();
                    addLetter(btn);
//                            }


                }

            }
        });
        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (onSelectCell((Button) v, event)) return true;
                return false;
            }
        });
        if (i == Math.ceil(size / 2)) {

            String s = String.valueOf(word_array[j]);
            btn.setText(s);
            btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);


        }

        flexboxRow.addView(btn);
        return btn;

    }

    private boolean isAnyNearEmptyCell(int currentStr, int currentCol) {

        return (currentCol - 1 >= 0 && !btns.get(currentStr).get(currentCol - 1).getText().toString().isEmpty()) ||
                (currentCol +1 < sizeOriginWord && !btns.get(currentStr).get(currentCol + 1).getText().toString().isEmpty()) ||
                (currentStr -1 >= 0 && !btns.get(currentStr - 1).get(currentCol).getText().toString().isEmpty()) ||
                (currentStr + 1 < sizeOriginWord && !btns.get(currentStr + 1).get(currentCol).getText().toString().isEmpty());
    }

    private boolean onSelectCell(Button v, MotionEvent event) {
        Button b = v;
        if (!b.getText().toString().isEmpty()) { // есть буква
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //this is touch
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                //this is click. Do everything triggered by a click here
                if (b.isPressed()) {
//                                    b.setPressed(false);
                    cancelSelect();

                } else {


                    TableTag tag = (TableTag) b.getTag();
                    if (previousTag != null) {
                        if ((Math.abs(tag.getI() - previousTag.getI()) == 1 || Math.abs(tag.getJ() - previousTag.getJ()) == 1)
                                && (!(Math.abs(tag.getI() - previousTag.getI()) == 1) || !(Math.abs(tag.getJ() - previousTag.getJ()) == 1))) {
                            include(b, tag);
                        } else {
                            if ((Math.abs(tag.getI() - previousTag.getI()) == 1) && (Math.abs(tag.getJ() - previousTag.getJ()) == 1)) {

                            }
                            cancelSelect();
                        }
                    } else {
                        include(b, tag);
                    }


                }
            }
            return true;

        }
        return false;
    }
    private void displayVictory(Context context) {
        DialogHelper dialogHelper = new DialogHelper(context);

        if (scoreP1 > scoreP2) {
            dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - игрок 1 со счётом - " + scoreP1);

        } else {
            if (scoreP1 < scoreP2) {
                dialogHelper.initializeVictoryDialog("Вы проиграли! Счёт игрока 2 - " + scoreP2);

            } else {
                dialogHelper.initializeVictoryDialog("Ничья!");

            }
        }
    }
    private void displayVictoryByMember() {
        DialogHelper dialogHelper = new DialogHelper(this);

        if (!playerOneTurn){
                dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - игрок 1 со счётом - " + scoreP1);

        } else {
                dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - игрок 2 со счётом - " + scoreP2);


//                dialogHelper.initializeVictoryDialog("Ничья!");

        }
    }


    private void nextPlayer() {
        if (playerOneTurn) {
            tvPlayerMove.setText("Ход игрока 1");
        } else {
            tvPlayerMove.setText("Ход игрока 2");

        }
    }

    private void clearSelectedBtn() {
        if (selectedBtn != null) {
            selectedBtn.setText("");
            selectedBtn = null;
        }

        isAddBlocked = false;
        isLetterAdded = false;
    }

    private void nextTurn() {
        isAddBlocked = false;
        isLetterAdded = false;
    }

    private boolean addWord() {
        ArrayList<String> list = Dict.getWords(this);
        String finalWord = input_string;
        String finalWordWithCount = finalWord + " (" + finalWord.length() + ")";
        if (list.contains(finalWord) || list.contains(new StringBuilder(finalWord).reverse().toString())) {
            if (!finalWord.equals(originalWord) && !listItemsP1.contains(finalWord) && !listItemsP2.contains(finalWord)) {
                if (playerOneTurn) {
                    addItemsP1(finalWordWithCount);
                    scoreP1 += finalWord.length();
                    scoreP1TV.setText(String.valueOf(scoreP1));
                    playerOneTurn = !playerOneTurn;

                } else {
                    addItemsP2(finalWordWithCount);
                    scoreP2 += finalWord.length();
                    scoreP2TV.setText(String.valueOf(scoreP2));

                    playerOneTurn = !playerOneTurn;

                }
                input_string = "";
                countOfBusyCells++;
                return true;
            } else {
                Toast.makeText(this, finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
            Toast.makeText(this, finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
                return false;
        }
    }

    private void addLetter(Button b) {
        Intent intent = new Intent(this, LetterInputActivity.class);
        //intent.putStringArrayListExtra("words", words);
        selectedBtn = b;
        startActivityForResult(intent, 2); // Activity code

    }

    private void include(Button b, TableTag tag) {
        b.setPressed(true);
        input_string += b.getText().toString();
//        inputChars.add()
        pressedBtns.add(b);
        previousTag = tag;
    }

    private void cancelSelect() {
        input_string = "";
        for (Button btn : pressedBtns) {
            btn.setPressed(false);
        }
        pressedBtns.clear();
        previousTag = null;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (2): {
                if (resultCode == Activity.RESULT_OK) {
                    // Extract the data returned from the childs Activity.

                    String input_letter = data.getStringExtra("letter");
                    selectedBtn.setText(input_letter);
                    isAddBlocked = true;
                    isLetterAdded = true;

                }
                break;
            }
        }
    }

    public void addItemsP1(String s) {
        listItemsP1.add(s);
        adapterP1.notifyDataSetChanged();
    }

    public void addItemsP2(String s) {
        listItemsP2.add(s);
        adapterP2.notifyDataSetChanged();
    }
}
