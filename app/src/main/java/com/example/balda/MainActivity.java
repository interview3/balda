package com.example.balda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.balda.Utils.DialogHelper;

public class MainActivity extends AppCompatActivity {
    public static int ONE = 1;
    public static int TWO = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeMenu();
    }

    private void initializeMenu() {
        Button mButtonOne = findViewById(R.id.btn_1_player);
        mButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SettingsActivity.class);
                intent.putExtra("NumberOfPlayers", ONE);
                startActivity(intent);

            }
        });

        Button mButtonTwo = findViewById(R.id.btn_2_player);
        mButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SettingsActivity.class);
                intent.putExtra("NumberOfPlayers", TWO);
                startActivity(intent);

            }
        });

        Button mButtonAbout = findViewById(R.id.about_program);
        mButtonAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogHelper dialogHelper = new DialogHelper(v.getContext());
                dialogHelper.initializeAboutDialog();

            }
        });

        Button mButtonRules = findViewById(R.id.btn_rules);
        mButtonRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogHelper dialogHelper = new DialogHelper(v.getContext());
                dialogHelper.initializeRulesDialog();

            }
        });
    }
}
