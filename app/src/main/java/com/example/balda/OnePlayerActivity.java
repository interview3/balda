package com.example.balda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balda.Models.TableTag;
import com.example.balda.Utils.AI;
import com.example.balda.Utils.DialogHelper;
import com.example.balda.Utils.Dict;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;

import java.util.ArrayList;
import java.util.Random;

public class OnePlayerActivity extends AppCompatActivity {
    String input_string = "";
    ArrayList<Button> pressedBtns = new ArrayList<>();
    ArrayList<ArrayList<Button>> btns = new ArrayList<>();
    ArrayList<Character> inputChars = new ArrayList<>();
    boolean playerOneTurn;
    ArrayAdapter<String> adapterP1;
    ArrayAdapter<String> adapterComputer;
    ArrayList<String> listItemsP1 = new ArrayList<String>();
    ArrayList<String> listItemsComputer = new ArrayList<String>();
    int scoreP1 = 0;
    int scoreComputer = 0;
    Button selectedBtn;
    String originalWord;
    TextView scoreP1TV;
    TextView scoreComputerTV;
    TableTag previousTag;
    boolean isAddBlocked;
    TextView tvPlayerMove;
    boolean isLetterAdded;
    int countOfBusyCells;
    AI computer;
    int sizeOriginWord;

    char[] alfavit = {'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р',
            'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_player);

        initializeUI();

        prepareAI();


        countOfBusyCells = sizeOriginWord;
    }

    private void prepareAI() {
        computer = new AI();
        if (!playerOneTurn) {
            computerTurn();
        }
    }

    private void initializeUI() {
        adapterP1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItemsP1);
        adapterComputer = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItemsComputer);
        ListView listView = findViewById(R.id.used_words_p1);
        listView.setAdapter(adapterP1);
        listView = findViewById(R.id.used_words_computer);
        listView.setAdapter(adapterComputer);

        scoreP1TV = findViewById(R.id.player_1_score);
        scoreComputerTV = findViewById(R.id.computer_score);

        Random rand = new Random();
        playerOneTurn = rand.nextBoolean();
        tvPlayerMove = findViewById(R.id.tv_player_move);
        if (playerOneTurn) {
            tvPlayerMove.setText("Ход игрока 1");
            Toast.makeText(this, "Ход игрока 1", Toast.LENGTH_LONG).show();
        } else {
            tvPlayerMove.setText("Ход компьютера");
            Toast.makeText(this, "Ход компьютера", Toast.LENGTH_LONG).show();

        }
        Intent intent = getIntent();
        originalWord = intent.getStringExtra("word");
        sizeOriginWord = originalWord.length();
        char[] word_array = originalWord.toCharArray();


        initializeGameField(word_array);

        Button OK = findViewById(R.id.btn_OK);
        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(countOfBusyCells == sizeOriginWord * sizeOriginWord)) {

                    if (isLetterAdded) {
                        boolean wordAdded = false;
                        wordAdded = addWord(v);
                        cancelSelect();
                        nextTurn();
//                            computer.turn(); // TODO if addword true computer turn
                        if (wordAdded) computerTurn();
                        else clearSelectedBtn();
                    } else {
                        Toast.makeText(v.getContext(), "Вы не добавили букву!", Toast.LENGTH_LONG).show();
                    }

                } else {
                    displayVictory(v.getContext());
                }
            }

        });

        Button reset = findViewById(R.id.btn_reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelectedBtn();
                cancelSelect();
            }
        });
        Button sur = findViewById(R.id.btn_sur);
        sur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                computerTurn();
                DialogHelper dialogHelper = new DialogHelper(v.getContext());
                displayVictoryByMember(v.getContext());


            }
        });
    }

    private void initializeGameField(char[] word_array) {
        FlexboxLayout parentGameFB = findViewById(R.id.parent_game_fb);
        FlexboxLayout flexboxRow = new FlexboxLayout(this);
        FlexboxLayout.LayoutParams params = new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT);
        flexboxRow.setLayoutParams(params);
        flexboxRow.setFlexWrap(FlexWrap.NOWRAP);
        int buttonStyle = R.style.Widget_AppCompat_Button_Colored;

        for (int i = 0; i < sizeOriginWord; i++) {
            ArrayList<Button> btnRow = new ArrayList<>();
            for (int j = 0; j < sizeOriginWord; j++) {
                btnRow.add(createCell(sizeOriginWord, word_array, flexboxRow, buttonStyle, i, j));

            }
            parentGameFB.addView(flexboxRow);
            flexboxRow = new FlexboxLayout(this);
            flexboxRow.setLayoutParams(params);
            flexboxRow.setFlexWrap(FlexWrap.NOWRAP);

            btns.add(btnRow);

        }
    }


    private void computerTurn() {

        int icurHorStr = (int) Math.ceil(sizeOriginWord / 2);
//            ArrayList<Button> btnsOriginalStr = btns.get(mainIndex);
//            String firstLetter = "";
//            boolean isLettersFound = false;
        boolean isFoundLetters = false;
//            int icurHorStr;
//            int icurVertStr;
        for (int icurVertStr = 0; icurVertStr < sizeOriginWord; icurVertStr++) {
//                firstLetter = btnsOriginalStr.get(f).getText().toString();
            //computer.incCurLength();

//                for (int i = 0; i < AI.DEFAULT_LENGTH; i++) {
//                icurHorStr = mainIndex;
            isFoundLetters = searchNextLetters(icurHorStr, icurVertStr, computer.getCurLength());
            usedVertIndexesByCell.clear();
            usedHorIndexesByCell.clear();
            if (isFoundLetters) { // found
//                    isLettersFound = true;
                break;
            } else {
                computer.resetCurLength();

            }

        }
        if (isFoundLetters) {
//                hasWord(isFoundLetters);
//                if (!addWordByComputer()) {
//                    computerTurn();
//                }
            nextPlayer();


        } else {
            displayVictoryByMember(this);
            // TODO GAME OVER
        }
//            computer.turn()

    }

    int rec;
    //    String searchableWord = "";
    String resultComputerWord;

    //    private boolean hasWord(int icurHorStr, int icurVertStr, int curLength, String searchableWord) {
//        if (curLength >= AI.getDefaultLength() && Dict.list.contains(searchableWord)
//        && !listItemsComputer.contains(searchableWord)) {
//            resultComputerWord = searchableWord;
//            if (addWordByComputer())
//            return true;
//        }
//        rec++;
//        if (icurHorStr + 1 < sizeOriginWord) {
//            {
//                Button checkingBtn = btns.get(icurHorStr + 1).get(icurVertStr);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//                    boolean check = Dict.trie.startsWith(searchableWord);
//                    if (check) {
//                        if (hasWord(icurHorStr + 1, icurVertStr, ++curLength, searchableWord))
//                            return true;
//                    }
//
//
////                    for (int i = 0; i < alfavit.length; i++) {
////                        Dict.trie.search(alfavit[i] + searchableWord);
////                    }
////                    curLength++;
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        if (icurHorStr - 1 >= 0) {
//            {
//                Button checkingBtn = btns.get(icurHorStr - 1).get(icurVertStr);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//
//                    if (Dict.trie.startsWith(searchableWord)) {
//
//                        if(hasWord(icurHorStr - 1, icurVertStr, ++curLength, searchableWord))
//                        return true;
//
//                    }
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        if (icurVertStr + 1 < sizeOriginWord) {
//            {
//                Button checkingBtn = btns.get(icurHorStr).get(icurVertStr + 1);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//
//                    if (Dict.trie.startsWith(searchableWord)) {
//                        if(hasWord(icurHorStr, icurVertStr + 1, ++curLength, searchableWord))
//                        return true;
//
//                    }
//
//
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        if (icurVertStr - 1 >= 0) {
//            {
//                Button checkingBtn = btns.get(icurHorStr).get(icurVertStr - 1);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//
//                    if (Dict.trie.startsWith(searchableWord)) {
//                        if(hasWord(icurHorStr, icurVertStr - 1, ++curLength, searchableWord))
//                        return true;
//
//                    }
//
//
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        return false;
//    }
    private boolean hasWord(int icurHorStr, int icurVertStr, int curLength, String searchableWord) {
        if (curLength >= AI.getDefaultLength() && Dict.list.contains(searchableWord)
                && !listItemsComputer.contains(searchableWord)) {
            resultComputerWord = searchableWord;
            if (addWordByComputer())
                return true;
        }
        rec++;
        if (icurHorStr + 1 < sizeOriginWord) {
            {
                if (hasWordByStep(icurHorStr, icurVertStr, curLength, 1, 0, searchableWord)) {

                    return true;

                }


            }
        }

        if (icurHorStr - 1 >= 0) {
            {
                if (hasWordByStep(icurHorStr, icurVertStr, curLength, -1, 0, searchableWord)) {

                    return true;
                }

            }
        }

        if (icurVertStr + 1 < sizeOriginWord) {
            {
                if (hasWordByStep(icurHorStr, icurVertStr, curLength, 0, 1, searchableWord)) {

                    return true;
                }

            }
        }

        if (icurVertStr - 1 >= 0) {
            {
                if (hasWordByStep(icurHorStr, icurVertStr, curLength, 0, -1, searchableWord)) {

                    return true;
                }

//                        else {
//                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
//
//                    }
            }
        }
        usedHorIndexesByWord.clear();
        usedVertIndexesByWord.clear();
        return false;
    }

    private boolean hasWordByStep(int icurHorStr, int icurVertStr, int curLength, int horStep, int verStep, String searchableWord) {
        if (!usedHorIndexesByWord.contains(icurHorStr+ horStep) && !usedVertIndexesByWord.contains(icurVertStr+ verStep)) {
            Button checkingBtn = btns.get(icurHorStr + horStep).get(icurVertStr + verStep);
            if (!checkingBtn.getText().toString().equals("")) {

                searchableWord += checkingBtn.getText().toString();
                boolean check = Dict.tree.findPref(searchableWord);
                if (check) {
                    usedHorIndexesByWord.add(icurHorStr);
                    usedVertIndexesByWord.add(icurVertStr);
                    if (hasWord(icurHorStr + horStep, icurVertStr + verStep, ++curLength, searchableWord))
                        return true;

                }


            }

        }

        return false;
    }

    //    private boolean hasWord(int icurHorStr, int icurVertStr, int curLength, String aalphavitChar, String ssearchableWord) {
//
//        if (curLength >= AI.getDefaultLength() && Dict.list.contains(searchableWord)
//                && !listItemsComputer.contains(searchableWord)) {
//            resultComputerWord = searchableWord;
//            if (addWordByComputer())
//                return true;
//        }
//        rec++;
//        if (icurHorStr + 1 < sizeOriginWord) {
//            {
//                Button checkingBtn = btns.get(icurHorStr + 1).get(icurVertStr);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
////                    searchableWord +=aalphavitChar;
//                    boolean check = Dict.trie.startsWith(searchableWord+aalphavitChar);
//                    if (check) {
//                        if (hasWord(icurHorStr + 1, icurVertStr, ++curLength,aalphavitChar, searchableWord+aalphavitChar))
//                            return true;
//                    }
//
//            }
//        }
//
//        if (icurHorStr - 1 >= 0) {
//            {
//                Button checkingBtn = btns.get(icurHorStr - 1).get(icurVertStr);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//
//                    if (Dict.trie.startsWith(searchableWord)) {
//
//                        if(hasWord(icurHorStr - 1, icurVertStr, ++curLength, searchableWord))
//                            return true;
//
//                    }
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        if (icurVertStr + 1 < sizeOriginWord) {
//            {
//                Button checkingBtn = btns.get(icurHorStr).get(icurVertStr + 1);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//
//                    if (Dict.trie.startsWith(searchableWord)) {
//                        if(hasWord(icurHorStr, icurVertStr + 1, ++curLength, searchableWord))
//                            return true;
//
//                    }
//
//
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        if (icurVertStr - 1 >= 0) {
//            {
//                Button checkingBtn = btns.get(icurHorStr).get(icurVertStr - 1);
//                if (!checkingBtn.getText().toString().equals("")) {
//                    searchableWord += checkingBtn.getText().toString();
//
//                    if (Dict.trie.startsWith(searchableWord)) {
//                        if(hasWord(icurHorStr, icurVertStr - 1, ++curLength, searchableWord))
//                            return true;
//
//                    }
//
//
//                }
////                        else {
////                        hasWord(icurHorStr + 1, icurVertStr, curLength, searchableWord);
////
////                    }
//            }
//        }
//
//        return false;
//    }
    private void displayVictoryByMember(Context context) {
        DialogHelper dialogHelper = new DialogHelper(context);
        if (!playerOneTurn) {
            dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - игрок 1 со счётом - " + scoreP1);
        } else {
            dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - компьютер со счётом - " + scoreComputer);
        }

    }

    private void displayVictory(Context context) {
        DialogHelper dialogHelper = new DialogHelper(context);
//        if (!playerOneTurn){
//            dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - игрок 1 со счётом - " + scoreP1);
//        } else {
//            dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - компьютер со счётом - " + scoreComputer);
//        }
        if (scoreP1 > scoreComputer) {
            dialogHelper.initializeVictoryDialog("Поздравляем! Победитель - игрок 1 со счётом - " + scoreP1);

        } else {
            if (scoreP1 < scoreComputer) {
                dialogHelper.initializeVictoryDialog("Вы проиграли! Счёт компьютера - " + scoreComputer);

            } else {
                dialogHelper.initializeVictoryDialog("Ничья!");

            }
        }
    }

    ArrayList<Integer> usedVertIndexesByCell = new ArrayList<>();
    ArrayList<Integer> usedHorIndexesByCell = new ArrayList<>();
    ArrayList<Integer> usedVertIndexesByWord = new ArrayList<>();
    ArrayList<Integer> usedHorIndexesByWord = new ArrayList<>();

    private boolean searchNextLetters(int icurHorStr, int icurVertStr, int curLenght) { //  int curLenght
//        if (computer.getCurLength() == AI.DEFAULT_LENGTH){
//            return searchableWord;
//        }

        if (!usedHorIndexesByCell.contains(icurHorStr) && !usedVertIndexesByCell.contains(icurVertStr)) {
            if (icurHorStr + 1 < sizeOriginWord) {
                {
                    if (checkNextCell(icurHorStr, icurVertStr, curLenght, 1, 0)) return true;
                }
            }
            if (icurHorStr - 1 >= 0) {
                {
                    if (checkNextCell(icurHorStr, icurVertStr, curLenght, -1, 0))
                        return true;
                }
            }
            if (icurVertStr + 1 < sizeOriginWord) {
                {
                    if (checkNextCell(icurHorStr, icurVertStr, curLenght, 0, 1))
                        return true;
                }
            }
            if (icurVertStr - 1 >= 0) {
                {
                    if (checkNextCell(icurHorStr, icurVertStr, curLenght, 0, -1))
                        return true;
                }
            }
        }
        return false;
    }

    private boolean checkNextCell(int icurHorStr, int icurVertStr, int curLenght, int stepHor, int stepVert) {
        Button checkingBtn = btns.get(icurHorStr + stepHor).get(icurVertStr + stepVert);
        if (checkingBtn.getText().toString().equals("")) {
            for (int i = 0; i < alfavit.length; i++) {

                if (hasWord(icurHorStr + stepHor, icurVertStr + stepVert, curLenght + 1, String.valueOf(alfavit[i]))) {
                    btns.get(icurHorStr + stepHor).get(icurVertStr + stepVert).setText(String.valueOf(alfavit[i]));
                    return true;
                }
            }
        } else {
            usedHorIndexesByCell.add(icurHorStr);
            usedVertIndexesByCell.add(icurVertStr);
            if (searchNextLetters(icurHorStr + stepHor, icurVertStr + stepVert, curLenght))
                return true;

        }
        return false;
    }
// startwith trie from 2 chars
//    private String searchUpToRight(int strIndex, String searchableWord, Button checkingBtn, int step) {
//        if (computer.getCurLength() == AI.DEFAULT_LENGTH){ // || strIndex +1 / -1 == array.length
//            return searchableWord;
//        }
//        Button checkingBtn = btns.get(strIndex + 1).get(curLenght);
//
//        searchableWord += checkingBtn.getText().toString();
//        computer.incCurLength();
//        return searchableWord;
//    }

    private boolean addWord(View v) {
        ArrayList<String> list = Dict.getWords(v.getContext());
        String finalWord = input_string;
        String finalWordWithCount = finalWord + " (" + finalWord.length() + ")";
        if (list.contains(finalWord) || list.contains(new StringBuilder(finalWord).reverse().toString())) {
            if (!finalWord.equals(originalWord) && !listItemsP1.contains(finalWord) && !listItemsComputer.contains(finalWord)) {
                if (playerOneTurn) {
                    addItemsP1(finalWordWithCount);
                    scoreP1 += finalWord.length();
                    scoreP1TV.setText(String.valueOf(scoreP1));
                    playerOneTurn = !playerOneTurn;

                } else {
//                    addItemsComputer(finalWordWithCount);
//                    scoreComputer += finalWord.length();
//                    scoreComputerTV.setText(String.valueOf(scoreComputer));
//
//                    playerOneTurn = !playerOneTurn;

                }
                input_string = "";
                countOfBusyCells++;
                return true;
            } else {
                Toast.makeText(v.getContext(), finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
            Toast.makeText(v.getContext(), finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
            return false;

        }
    }

    private boolean addWordByComputer() {
        ArrayList<String> list = Dict.getWords(this);
        String finalWord = resultComputerWord;
        String finalWordWithCount = finalWord + " (" + finalWord.length() + ")";
        if (list.contains(finalWord) || list.contains(new StringBuilder(finalWord).reverse().toString())) {
            if (!finalWord.equals(originalWord) && !listItemsP1.contains(finalWord) && !listItemsComputer.contains(finalWord)) {
                if (playerOneTurn) {
//                    addItemsP1(finalWordWithCount);
//                    scoreP1 += finalWord.length();
//                    scoreP1TV.setText(String.valueOf(scoreP1));
//                    playerOneTurn = !playerOneTurn;

                } else {
                    addItemsComputer(finalWordWithCount);
                    scoreComputer += finalWord.length();
                    scoreComputerTV.setText(String.valueOf(scoreComputer));

                    playerOneTurn = !playerOneTurn;

                }
                resultComputerWord = "";
                countOfBusyCells++;
                return true;
            } else {
                resultComputerWord = "";

//                Toast.makeText(this, finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
                return false;

            }
        } else {
            return false;
//            Toast.makeText(this, finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
        }
    }

    //    private void addWord(View v) {
//        ArrayList<String> list = Dict.getWords(v.getContext());
//        String finalWord = input_string;
//        String finalWordWithCount = finalWord + " (" + finalWord.length() + ")";
//        if (list.contains(finalWord) || list.contains(new StringBuilder(finalWord).reverse().toString())) {
//            if (!finalWord.equals(originalWord) && !listItemsP1.contains(finalWord) && !listItemsComputer.contains(finalWord)) {
//                if (playerOneTurn) {
//                    addItemsP1(finalWordWithCount);
//                    scoreP1 += finalWord.length();
//                    scoreP1TV.setText(String.valueOf(scoreP1));
//                    playerOneTurn = !playerOneTurn;
//
//                } else {
//                    addItemsComputer(finalWordWithCount);
//                    scoreComputer += finalWord.length();
//                    scoreComputerTV.setText(String.valueOf(scoreComputer));
//
//                    playerOneTurn = !playerOneTurn;
//
//                }
//                input_string = "";
//                countOfBusyCells++;
//            } else {
//                Toast.makeText(v.getContext(), finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
//
//            }
//        } else {
//            Toast.makeText(v.getContext(), finalWord + " - не существует или оно было", Toast.LENGTH_LONG).show();
//        }
//    }
    public void addItemsP1(String s) {
        listItemsP1.add(s);
        adapterP1.notifyDataSetChanged();
    }

    public void addItemsComputer(String s) {
        listItemsComputer.add(s);
        adapterComputer.notifyDataSetChanged();
    }

    private Button createCell(int size, char[] word_array, FlexboxLayout flexboxRow, int buttonStyle, int i, int j) {
        final Button btn = new Button(new ContextThemeWrapper(this, buttonStyle), null, buttonStyle);
        FlexboxLayout.LayoutParams btnParams = new FlexboxLayout.LayoutParams(FlexboxLayout.LayoutParams.WRAP_CONTENT, FlexboxLayout.LayoutParams.WRAP_CONTENT);
        btn.setLayoutParams(btnParams);
        btn.setGravity(Gravity.CENTER);
        btn.setTag(new TableTag(i, j));

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button) v;
                TableTag tag = (TableTag) btn.getTag();
                int currentStr = tag.getI();
                int currentCol = tag.getJ();
                if (isAnyNearEmptyCell(currentStr, currentCol))

                    if (btn.getText().toString().isEmpty() && !isAddBlocked) {
//                            if ((Math.abs(tag.getI() - previousTag.getI()) == 1 || Math.abs(tag.getJ() - previousTag.getJ()) == 1)
//                                    && (!(Math.abs(tag.getI() - previousTag.getI()) == 1) || !(Math.abs(tag.getJ() - previousTag.getJ()) == 1))) { // XOR
                        cancelSelect();
                        addLetter(v, btn);
//                            }


                    }

            }
        });
        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (onSelectCell((Button) v, event)) return true;
                return false;
            }
        });
        if (i == Math.ceil(size / 2)) {

            String s = String.valueOf(word_array[j]);
            btn.setText(s);
            btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);


        }

        flexboxRow.addView(btn);
        return btn;
    }

    private boolean isAnyNearEmptyCell(int currentStr, int currentCol) {

        return (currentCol - 1 >= 0 && !btns.get(currentStr).get(currentCol - 1).getText().toString().isEmpty()) ||
                (currentCol +1 < sizeOriginWord && !btns.get(currentStr).get(currentCol + 1).getText().toString().isEmpty()) ||
                (currentStr -1 >= 0 && !btns.get(currentStr - 1).get(currentCol).getText().toString().isEmpty()) ||
                (currentStr + 1 < sizeOriginWord && !btns.get(currentStr + 1).get(currentCol).getText().toString().isEmpty());
    }

    private void addLetter(View v, Button b) {
        Intent intent = new Intent(v.getContext(), LetterInputActivity.class);
        //intent.putStringArrayListExtra("words", words);
        selectedBtn = b;
        startActivityForResult(intent, 2); // Activity code

    }

    private void include(Button b, TableTag tag) {
        b.setPressed(true);
        input_string += b.getText().toString();
//        inputChars.add()
        pressedBtns.add(b);
        previousTag = tag;
    }

    private void cancelSelect() {
        input_string = "";
        for (Button btn : pressedBtns) {
            btn.setPressed(false);
        }
        pressedBtns.clear();
        previousTag = null;

    }

    private boolean onSelectCell(Button v, MotionEvent event) {
        Button b = v;
        if (!b.getText().toString().isEmpty()) { // есть буква
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //this is touch
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                //this is click. Do everything triggered by a click here
                if (b.isPressed()) {
//                                    b.setPressed(false);
                    cancelSelect();

                } else {


                    TableTag tag = (TableTag) b.getTag();
                    if (previousTag != null) {
                        if ((Math.abs(tag.getI() - previousTag.getI()) == 1 || Math.abs(tag.getJ() - previousTag.getJ()) == 1)
                                && (!(Math.abs(tag.getI() - previousTag.getI()) == 1) || !(Math.abs(tag.getJ() - previousTag.getJ()) == 1))) { // TODO добавить ИЛИ (||) соседняя пустая, ТО
                            include(b, tag);
                        } else {
                            if ((Math.abs(tag.getI() - previousTag.getI()) == 1) && (Math.abs(tag.getJ() - previousTag.getJ()) == 1)) {

                            }
                            cancelSelect();
                        }
                    } else {
                        include(b, tag);
                    }


                }
            }
            return true;

        }
        return false;
    }

    private void clearSelectedBtn() {
        if (selectedBtn != null) {
            selectedBtn.setText("");
            selectedBtn = null;
        }

        isAddBlocked = false;
        isLetterAdded = false;
    }

    private void nextPlayer() {
        if (playerOneTurn) {
            tvPlayerMove.setText("Ход игрока 1");
        }
    }

    private void nextTurn() {
        isAddBlocked = false;
        isLetterAdded = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (2): {
                if (resultCode == Activity.RESULT_OK) {
                    // Extract the data returned from the childs Activity.

                    String input_letter = data.getStringExtra("letter");
                    selectedBtn.setText(input_letter);
                    isAddBlocked = true;
                    isLetterAdded = true;

                }
                break;
            }
        }
    }
}
