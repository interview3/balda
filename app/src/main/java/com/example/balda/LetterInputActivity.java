package com.example.balda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class LetterInputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letter_input);
//        LinearLayout row = new LinearLayout(this);
//
//        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//        row.setOrientation(LinearLayout.HORIZONTAL);

//children of parent linearlayout

        int buttonStyle = R.style.Widget_AppCompat_Button_Colored;
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                16,
                getResources().getDisplayMetrics()
        );
        LinearLayout parentLL = findViewById(R.id.parent_letter_ll);

        char[] alfavit = {'А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
                'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'};

        LinearLayout linearRow = new LinearLayout(this);
        LinearLayout.LayoutParams parentParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        linearRow.setLayoutParams(parentParams);
        linearRow.setOrientation(LinearLayout.HORIZONTAL);
//        linearRow.setGravity(Gravity.CENTER_VERTICAL);
        int count = 0;
        for (char letter : alfavit)
        {
            if (count == 4){
                parentLL.addView(linearRow);

                linearRow = new LinearLayout(this);
                linearRow.setLayoutParams(parentParams);
                linearRow.setOrientation(LinearLayout.HORIZONTAL);
                count = 0;
            }
            Button btn = new Button(new ContextThemeWrapper(this, buttonStyle), null, buttonStyle);
            LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            btn.setLayoutParams(btnParams);
            btn.setGravity(Gravity.CENTER);
            String s = Character.toString(letter);
            btn.setText(s);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button b = (Button) v;
                    Intent resultIntent = new Intent();
// TODO Add extras or a data URI to this intent as appropriate.
                    resultIntent.putExtra("letter", b.getText().toString());
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
            });
            linearRow.addView(btn);

            if (s.equals("Я")) parentLL.addView(linearRow);

            count++;
        }


    }
}
