package com.example.balda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.balda.Utils.Dict;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Random;

public class SettingsActivity extends AppCompatActivity {
    TextInputLayout textInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

            initializeUI();


    }

    private void initializeUI() {
        textInputLayout = findViewById(R.id.size_textImput);
        final TextView word = findViewById(R.id.word);
        word.setText(getRandomWord());

        Button plus = findViewById(R.id.plus);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeWord(v, word);
            }


        });


        Button mButtonOK = findViewById(R.id.start_btn);
        mButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                startGame(v.getContext(), word);
            } catch (Exception e){
                Toast.makeText(v.getContext(), "Неверный ввод данных!", Toast.LENGTH_LONG).show();

            }
            }
        });
    }

    private String getRandomWord() {
        String randomElement = null;
        final ArrayList<String> list = Dict.getWords(this);

        final int size = Integer.parseInt(textInputLayout.getEditText().getText().toString());
        if (size >= 3 && (size%2 == 1)) {

            do {
            Random rand = new Random();
            randomElement = list.get(rand.nextInt(list.size()));
        } while (randomElement.length() != size);
        } else {
            Toast.makeText(this, "Неверный размер поля", Toast.LENGTH_LONG).show();
        }
        return randomElement;
    }

    private void startGame(Context context, TextView word) {
        textInputLayout = findViewById(R.id.size_textImput);
        int size = Integer.parseInt(textInputLayout.getEditText().getText().toString());
        Intent intent = getIntent();
        int numberOfPlayers = intent.getIntExtra("NumberOfPlayers", 0);
        Intent newIntent;
        if (numberOfPlayers == MainActivity.ONE){
            newIntent = new Intent(context, OnePlayerActivity.class);

        } else {
            newIntent = new Intent(context, TwoPlayersActivity.class);

        }
//                newIntent.putExtra("word", "АБЕРРАЦИЯ");
        newIntent.putExtra("word", word.getText().toString());
        newIntent.putExtra("size", size);
        startActivity(newIntent);
    }

    private void changeWord(View v, TextView word) {

            word.setText(getRandomWord());

    }


}
